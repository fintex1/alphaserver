-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Agu 2022 pada 18.42
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fintexinterview`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `node`
--

CREATE TABLE `node` (
  `id_node` int(11) NOT NULL,
  `name_node` varchar(64) NOT NULL,
  `attemps` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `node`
--

INSERT INTO `node` (`id_node`, `name_node`, `attemps`) VALUES
(1, 'Node Example', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sshd`
--

CREATE TABLE `sshd` (
  `id` int(11) NOT NULL,
  `log` varchar(128) NOT NULL,
  `node` varchar(32) NOT NULL,
  `user` varchar(64) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1=success, 2=failed',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`) VALUES
(1, 'bintang', '$2y$10$119bdWzUsh7aBLXbleSlUeSzuYiFI6kfJieFSyLUDcitanh1vQGtm');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `node`
--
ALTER TABLE `node`
  ADD PRIMARY KEY (`id_node`);

--
-- Indeks untuk tabel `sshd`
--
ALTER TABLE `sshd`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `node`
--
ALTER TABLE `node`
  MODIFY `id_node` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `sshd`
--
ALTER TABLE `sshd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
