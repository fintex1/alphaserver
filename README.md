### Installation Of AlphaServer ###
1. Send Folder AlphaServer to /opt folder
```
cd /opt
git clone https://gitlab.com/fintex1/alphaserver/
cd alphaserver/
chmod +x install.sh
```

2. Run 
```./install.sh ```
follow instruction for install AlphaServer on Main Server

3. Get Access Server From Script
with login default:

- u : bintang
- p : bintang

4. Get Access SSH Monitor 
5. If AlphaClient Installed, Dashboard Automatically Sync With Node

### Dashboard Demo Access
<a href="http://104.154.150.135/alphaserver/home/dashboard">http://104.154.150.135/alphaserver/home/dashboard</a>

```
u : bintang
p : bintang
```

<img src="img/demo.png">