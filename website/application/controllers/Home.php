<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function index()
	{
		has_loggedin();
		set_rules('username', 'username', 'required');
		set_rules('password', 'password', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('login');
		} else {
			$data = dbgetwhere('user', ['username' => post('username')])->row_array();
			if ($data) {
				if (password_verify(post("password"), $data['password'])) {
					set_userdata('username', $data['username']);
					set_userdata('login', true);
					set_flashdata('msg', swalfire('Berhasil Login'));
					redirect(base_url('home/dashboard'));
				} else {
					set_flashdata('msg', swalfire('Password Salah', 'error'));
					redirect(base_url('home'));
				}
			} else {
				set_flashdata('msg', swalfire('User Tidak Terdaftar', 'error'));
				redirect(base_url('home'));
			}
		}
	}
	// public function register()
	// {
	// 	set_rules('username', 'username', 'required');
	// 	set_rules('password', 'password', 'required');
		
	// 	if ($this->form_validation->run() == false) {
	// 		$this->load->view('register');
	// 	} else {
	// 		$data = [
	// 			'username' => post('username'),
	// 			'password' => password_hash(post('password'), PASSWORD_DEFAULT),
	// 		];
	// 		dbinsert('user', $data);
	// 		if ($data) {
	// 			if (password_verify(post("password"), $data['password'])) {
	// 				set_userdata('username', $data['username']);
	// 				set_userdata('login', true);
	// 				set_flashdata('msg', swalfire('Berhasil Login'));
	// 				redirect(base_url('home/dashboard'));
	// 			} else {
	// 				set_flashdata('msg', swalfire('Password Salah', 'error'));
	// 				redirect(base_url('home'));
	// 			}
	// 		} else {
	// 			set_flashdata('msg', swalfire('User Tidak Terdaftar', 'error'));
	// 			redirect(base_url('home'));
	// 		}
	// 	}
	// }
	public function logout()
	{
		unset_userdata('username');
		unset_userdata('login');
		set_flashdata('msg', swalfire('Berhasil Logout'));
		redirect(base_url('home'));
	}
	public function dashboard()
	{
		not_has_loggedin();
		view('dashboard');
	}
	public function attemps()
	{
		not_has_loggedin();
		$data = dbget('node')->result_array();
		echo jsonify($data);
	}
	public function regnode($apikey='')
	{
		if ($apikey == '9875badc798b5ad78cb5978dac9a78cb97a9dcdba9') {
			set_rules("name", "name", "required|numeric");

			if ($this->form_validation->run() == false) {
				echo 'dump';
				// echo validation_errors();
			} else {
				$get = dbgetwhere('node', ['id_node' => post('node')])->row_array();
				dbinsert('node', ['attemps' => $get['attemps']+1], ['id_node' => post("node")]);
				echo jsonify(['code' => 200, 'msg' => 'ok']);
			}
		} else {
			echo 'who are you';
		}
	}
	public function api($apikey='')
	{
		if ($apikey == '9875badc798b5ad78cb5978dac9a78cb97a9dcdba9') {
			set_rules("name_node", "name_node", "required");

			if ($this->form_validation->run() == false) {
				echo 'dump';
				// echo validation_errors();
			} else {
				$get = dbgetwhere('node', ['name_node' => post('name_node')])->row_array();
				if ($get){
					dbupdate('node', ['attemps' => $get['attemps']+1], ['name_node' => post("name_node")]);
					echo jsonify(['code' => 200, 'msg' => 'ok']);
				} else {
					dbinsert('node', ['name_node' => post('name_node'), 'attemps' => 1]);
					echo jsonify(['code' => 200, 'msg' => 'ok']);
				}
			}
			// set_rules("log", "log", "required");
			// set_rules("node", "node", "required");
			// set_rules("user", "user", "required");
			// set_rules("type", "type", "required");

			// if ($this->form_validation->run() == false) {
			// 	echo 'dump';
			// 	// echo validation_errors();
			// } else {
			// 	$data = [
			// 		"log" => post("log"),
			// 		"node" => post("node"),
			// 		"user" => post("user"),
			// 		"type" => post("type"),
			// 	];
			// 	echo jsonify($data);
			// }
		} else {
			echo 'who are you';
		}
	}
}
