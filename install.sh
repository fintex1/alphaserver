#!/bin/bash
# Minimum requirements:
# - Ubuntu 18.04 LTS [Tested]

# check pwd on /opt/alphaserver
if [ "$(pwd)" != "/opt/alphaserver" ]; then
    echo "Folder Should be on /opt/alphaserver (Case Sensitive Implemented)"
    exit 1
fi

# check root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi


# check php installed
if [ -f /etc/php/7.4/apache2/php.ini ]; then
    echo "PHP is installed"
else
    echo "PHP is not installed"
    echo "Installing PHP"
    sudo apt-get update
    sudo apt-add-repository ppa:ondrej/php -y
    sudo apt-get update
    sudo apt-get install php7.4 php7.4-mysql php7.4-mbstring php7.4-xml php7.4-gd php7.4-intl php7.4-zip php7.4-curl -y
fi

# check apache2 installed
if [ -f /etc/apache2/apache2.conf ]; then
    echo "Apache2 is installed"
else
    echo "Apache2 is not installed"
    echo "Installing Apache2"
    sudo apt-get install apache2 phpmyadmin -y
    sudo service apache2 restart
    echo "<Directory /var/www/>
            Options Indexes FollowSymLinks
            AllowOverride all
            Require all granted
    </Directory>" >> /etc/apache2/apache2.conf
    sudo a2enmod rewrite
    sudo service apache2 restart
fi

# check mariadb installed
if [ -f /etc/mysql/mariadb.cnf ]; then
    echo "MySQL is installed"
    # get username password mysql
    read -p "Enter username for MySQL: " username
    read -p "Enter password for MySQL: " password
else
    echo "MySQL is not installed"
    echo "Installing MySQL"
    sudo apt-get install mariadb-server -y
    # UPDATE mysql.user SET plugin = 'mysql_native_password', authentication_string = PASSWORD('abc123') WHERE User = 'root'; FLUSH Privileges;
    mysql -uroot -e "UPDATE mysql.user SET plugin = 'mysql_native_password', authentication_string = PASSWORD('root') WHERE User = 'root'; FLUSH PRIVILEGES;"
    username="root"
    password="root"
fi


# check connection mysql
if mysql -u$username -p$password -e "show databases;" ; then
    echo "MySQL is connected"
    echo "Creating database"
    mysql -u$username -p$password -e "CREATE DATABASE fintexinterview;"
    echo "Importing database"
    mysql -u$username -p$password --database=fintexinterview < /opt/alphaserver/fintexinterview.sql
else
    echo "MySQL is not connected"
    echo "Please check username and password"
    exit 1
fi

# copy website to apache2
if [ -d /var/www/html/alphaserver ]; then
    echo "Website is installed"
else
    echo "Website is not installed"
    echo "Installing Website"
    sudo cp -r /opt/alphaserver/website /var/www/html/alphaserver
fi

# copy env database
if [ -f /var/www/html/alphaserver/application/config/database.php ]; then
    echo "Environment DB is Already installed !"
else
    echo "Environment DB is not installed"
    echo "Installing Environment"
    sudo cp /var/www/html/alphaserver/application/config/database.php.env /var/www/html/alphaserver/application/config/database.php
    sed -i "s/MYUSERNAME/$username/g" /var/www/html/alphaserver/application/config/database.php
    sed -i "s/MYPASSWORD/$password/g" /var/www/html/alphaserver/application/config/database.php
    sed -i "s/MYDATABASE/fintexinterview/g" /var/www/html/alphaserver/application/config/database.php
fi

#get public ip server
# curl cilent mode

publicip=$(curl ipinfo.io/ip 2>/dev/null)
ip=$(hostname -I)
echo "if you want to access dashboard from ip public, please use this url"
echo "http://$publicip/alphaserver"
echo "If you want to access dashboard from ip local, please use this url"
echo "http://$ip/alphaserver"
echo "or"
echo "http://localhost/alphaserver"










